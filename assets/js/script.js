// lightbox starts here
var viewImage = document.querySelectorAll(".sale-gallery img");

for (var i=0;i<viewImage.length;i++) {
    viewImage[i].addEventListener("click",displayLightBox);
}

var closeButton = document.querySelector(".close-button")
closeButton.addEventListener("click",hideLightBox);

var lightBox = document.querySelector(".sale-gallery .wrapper div");
lightBox.addEventListener("click",hideLightBox);

function displayLightBox() {
    lightBox.classList.remove("hidden");
    lightBox.classList.add("lightbox");
    document.querySelector("html").classList.add("overflow-hidden");
    document.querySelector('.focus img').src = this.src;
}

function hideLightBox(e) {
	if (e.target === e.currentTarget || e.target.nodeName === "SPAN") {
		lightBox.classList.add("hidden");
		document.querySelector("html").classList.remove("overflow-hidden");
	}
}

document.addEventListener("keyup",keyPress);

function keyPress (e) {
    if(e.key == "Escape" || e.key == "Esc") {
    	lightBox.classList.add("hidden");
		document.querySelector("html").classList.remove("overflow-hidden");
    }
}

// lightbox ends here